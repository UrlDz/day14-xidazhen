package com.oocl.todolist.service;

import com.oocl.todolist.entity.Todo;
import com.oocl.todolist.repository.TodoJPARepository;
import com.oocl.todolist.service.dto.TodoRequet;
import com.oocl.todolist.service.dto.TodoResponse;
import com.oocl.todolist.service.mapper.TodoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoJPARepository todoJPARepository;

    public TodoService(TodoJPARepository todoJPARepository) {
        this.todoJPARepository = todoJPARepository;
    }

    public List<TodoResponse> getTodoList() {
        return todoJPARepository.findAll().stream().
                map((todo -> TodoMapper.toResponse(todo)))
                .collect(Collectors.toList());
    }

    public TodoResponse addTodo(TodoRequet todoRequet) {
        return TodoMapper.toResponse(todoJPARepository.save(TodoMapper.toEntity(todoRequet)));
    }

    public TodoResponse getTodoById(Long id){
        return TodoMapper.toResponse(todoJPARepository.findById(id).get());
    }

    public void deleteTodo(Long id) throws Exception {
        if (!todoJPARepository.findById(id).isEmpty()) {
            todoJPARepository.deleteById(id);
        }
    }

    public TodoResponse updateTodo(Long id, TodoRequet todoRequet) {
        Todo todo = TodoMapper.toEntity(todoRequet);
        Todo updateTodo = todoJPARepository.findById(id).get();
        if (todoRequet.getName() != null){
            updateTodo.setName(todo.getName());
        }
        if (todoRequet.getDone() != null){
            updateTodo.setDone(todo.getDone());
        }
        updateTodo = todoJPARepository.save(updateTodo);
        return TodoMapper.toResponse(updateTodo);
    }
}
