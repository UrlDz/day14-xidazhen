package com.oocl.todolist.service.dto;

public class TodoRequet {

    private String name;

    private Boolean done;

    public TodoRequet(String name, Boolean done) {
        this.name = name;
        this.done = done;
    }

    public TodoRequet(String name) {
        this.name = name;
    }

    public TodoRequet(Boolean done) {
        this.done = done;
    }

    public TodoRequet() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
}
