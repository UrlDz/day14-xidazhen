package com.oocl.todolist.controller;

import com.oocl.todolist.service.TodoService;
import com.oocl.todolist.service.dto.TodoRequet;
import com.oocl.todolist.service.dto.TodoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoController {
    @Autowired
    private TodoService todoService;

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public TodoResponse updateTodo(@PathVariable Long id, @RequestBody TodoRequet todoRequet) {
        return todoService.updateTodo(id, todoRequet);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable Long id) throws Exception {
        todoService.deleteTodo(id);
    }

    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable Long id) {
        return todoService.getTodoById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse addTodo(@RequestBody TodoRequet todoRequet) {
        return todoService.addTodo(todoRequet);
    }
    @GetMapping
    public List<TodoResponse> getTodoList() {
        return todoService.getTodoList();
    }





}
