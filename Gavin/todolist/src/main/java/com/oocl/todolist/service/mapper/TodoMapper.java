package com.oocl.todolist.service.mapper;

import com.oocl.todolist.entity.Todo;
import com.oocl.todolist.service.dto.TodoRequet;
import com.oocl.todolist.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;


public class TodoMapper {
    public static Todo toEntity(TodoRequet request) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(request, todo);
        return todo;
    }

    public static TodoResponse toResponse(Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }
}
