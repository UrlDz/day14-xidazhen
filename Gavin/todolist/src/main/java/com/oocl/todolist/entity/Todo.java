package com.oocl.todolist.entity;

import javax.persistence.*;

@Entity
@Table(name = "todo")
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Boolean done;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Todo() {
    }

    public Todo(String name, Boolean done) {
        this.name = name;
        this.done = done;
    }

    public Todo(Long id, String name, Boolean done) {
        this.id = id;
        this.name = name;
        this.done = done;
    }
}
