CREATE TABLE if NOT EXISTS todo
(
    id bigint not null auto_increment primary key,
    name varchar(255) not null,
    done boolean
);