package com.oocl.todolist.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oocl.todolist.entity.Todo;
import com.oocl.todolist.repository.TodoJPARepository;
import com.oocl.todolist.service.dto.TodoRequet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TodoJPARepository todoJPARepository;

    @BeforeEach
    void setUp() {
        todoJPARepository.deleteAll();
    }

    @Test
    void should_update_todo_name_when_use_updateTodo_given_only_name_changed() throws Exception {
        Todo todo = getTodo1();
        Todo actualTodo = todoJPARepository.save(todo);
        ObjectMapper objectMapper = new ObjectMapper();
        TodoRequet todoRequet = getTodoRequestOnlyName();
        String updatedTodoJson = objectMapper.writeValueAsString(todoRequet);
        mockMvc.perform(put("/todo/{id}", actualTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(204));
        Todo updateTodo = todoJPARepository.findById(actualTodo.getId()).get();
        assertTrue(updateTodo != null);
        Assertions.assertEquals(actualTodo.getId(), updateTodo.getId());
        Assertions.assertEquals(todoRequet.getName(), updateTodo.getName());
    }
    @Test
    void should_update_todo_done_when_use_updateTodo_given_only_done_changed() throws Exception {
        Todo todo = getTodo1();
        Todo actualTodo = todoJPARepository.save(todo);
        ObjectMapper objectMapper = new ObjectMapper();
        TodoRequet todoRequet = getTodoRequestOnlyDone();
        String updatedTodoJson = objectMapper.writeValueAsString(todoRequet);
        mockMvc.perform(put("/todo/{id}", actualTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(204));
        Todo updateTodo = todoJPARepository.findById(actualTodo.getId()).get();
        assertTrue(updateTodo != null);
        Assertions.assertEquals(actualTodo.getId(), updateTodo.getId());
        Assertions.assertEquals(todoRequet.getDone(), updateTodo.getDone());
    }

    @Test
    void should_delete_todo_when_use_deleteTodo_by_id() throws Exception {
        Todo todo = getTodo1();
        Long id = todoJPARepository.save(todo).getId();
        mockMvc.perform(delete("/todo/{id}", id))
                .andExpect(MockMvcResultMatchers.status().is(204));
        assertTrue(todoJPARepository.findById(id).isEmpty());
    }

    @Test
    void should_return_todo_response_by_id_when_use_getTodoById_interface_when_given_id() throws Exception {
        Todo todo = getTodo1();
        Long todoId = todoJPARepository.save(todo).getId();

        mockMvc.perform(get("/todo/{id}", todoId))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todoId))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todo.getName()));
    }

    @Test
    void should_add_todo_when_use_addTodo_interface() throws Exception {
        TodoRequet todoRequet = getTodoRequest1();
        ObjectMapper objectMapper = new ObjectMapper();
        String companyRequest = objectMapper.writeValueAsString(todoRequet);
        mockMvc.perform(post("/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todoRequet.getName()));
    }

    @Test
    void should_return_todo_responses_when_use_getTodoList_interface() throws Exception {
        Todo todo1 = getTodo1();
        Todo todo = todoJPARepository.save(todo1);
        mockMvc.perform(get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(todo.getName()));
    }


    private Todo getTodo1() {
        Todo todo = new Todo("todo1", false);
        return todo;
    }

    private TodoRequet getTodoRequest1() {
        TodoRequet todoRequet = new TodoRequet("todo1", true);
        return todoRequet;
    }

    private TodoRequet getTodoRequestOnlyDone() {
        TodoRequet todoRequet = new TodoRequet(true);
        return todoRequet;
    }
    private TodoRequet getTodoRequestOnlyName() {
        TodoRequet todoRequet = new TodoRequet("todo11111");
        return todoRequet;
    }
}