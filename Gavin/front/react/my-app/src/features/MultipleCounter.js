import CounterSizeGenerator from "./CounterSizeGenerator";
import CounterGroup from "./CounterGroup";
import CounterGroupSum from './CounterGroupSum';

const MultipleCounter = () => {

  return (
    <div>
      <CounterSizeGenerator />
      <CounterGroupSum />
      <CounterGroup />
    </div>
  );
};


export default MultipleCounter;