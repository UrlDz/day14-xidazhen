import Counter from './Counter';
import { useSelector, useDispatch } from 'react-redux'
import { updateCounterValue } from "./counterSlice"
const CounterGroup = () => {
  const counterList = useSelector(state => state.counter.counterList)
  const dispatch = useDispatch()
  const handleChange = (counterValue, index) => {
    dispatch(updateCounterValue({ counterValue, index }))
  };

  return (
    <div>
      {counterList.map((counter, index) => {
        return (
          <Counter
            key={index}
            counter={counter}
            updateCounterValue={(value) => handleChange(value, index)}
          />
        );
      })}
    </div>
  );
};

export default CounterGroup;
