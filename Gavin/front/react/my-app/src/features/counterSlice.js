import { createSlice } from "@reduxjs/toolkit";
const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        counterList: [1]
    },
    reducers: {
        updateCounterList: (state, action) => {
            state.counterList = action.payload
                ? Array.from({ length: action.payload }).fill(0)
                : [];
        },
        updateCounterValue: (state, action) => {
            const { counterValue, index } = action.payload
            state.counterList = state.counterList.map((oldCounterValue, itemIndex) => {
                if (itemIndex === index) {
                    return counterValue;
                } else {
                    return oldCounterValue;
                }
            });
        }
    },
});
export default counterSlice.reducer;
export const { updateCounterList, updateCounterValue } = counterSlice.actions;