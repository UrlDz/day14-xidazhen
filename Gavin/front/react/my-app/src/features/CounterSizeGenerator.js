import { useSelector, useDispatch } from 'react-redux'
import { updateCounterList } from './counterSlice';
const CounterSizeGenerator = () => {
  const counterList = useSelector(state => state.counter.counterList)
  const dispatch = useDispatch()
  const updateSize = (event) => {
    dispatch(updateCounterList(event.target.value))
  };
  return (
    <div>
      Size: <input type='number' value={counterList.length || ''} onChange={updateSize} />
    </div>
  );
};

export default CounterSizeGenerator;