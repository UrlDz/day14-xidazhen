const Counter = (props) => {
  const { counter, updateCounterValue } = props;

  const increase = () => {
    updateCounterValue(counter + 1);
  };

  const decrease = () => {
    updateCounterValue(counter - 1);
  };

  return (
    <div className='counter'>
      <button onClick={increase}>+</button>
      <h3>{counter}</h3>
      <button onClick={decrease}>-</button>
    </div>
  );
};

export default Counter;
