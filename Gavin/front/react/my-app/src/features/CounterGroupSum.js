import { useSelector } from "react-redux";

const CounterGroupSum = (props) => {
  const counterList = useSelector((state) => state.counter.counterList)
  const sum = counterList.reduce((sum,currentValue)=>{
    return sum+currentValue
  },0)
  return <>Sum:{sum}</>;
};

export default CounterGroupSum;
