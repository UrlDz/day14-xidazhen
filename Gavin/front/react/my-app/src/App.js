
import './App.css';
import MultipleCounter from './features/MultipleCounter';


function App() {
  return <div className="App">
    <MultipleCounter/>
  </div>;
}

export default App;
