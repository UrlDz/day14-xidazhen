package java.com.oocl.packagebooking.service;

import com.oocl.packagebooking.entity.Pack;
import com.oocl.packagebooking.repository.PackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PackService {
    @Autowired
    PackRepository packRepository;
    public Pack addPackage(Pack pack) {
        System.out.println(pack.getRegisterTime());
        return packRepository.save(pack);
    }

    public List<Pack> getAllPackage(){
        return packRepository.findAll();
    }

    public Pack modifyPackage(long id) {
        Optional<Pack> optional = packRepository.findById(id);
        Pack pack =null;
        if(optional.isPresent()){
            pack = optional.get();
            if(isTimePermitted(10)) {
                pack.setState("已取件");
            }
            return  packRepository.save(pack);
        }
        return null;
    }
    public boolean isTimePermitted(int time){
        if(time>9&&time<20)
            return true;
        return false;
    }
    public List<Pack> getHavingPack(){
        List<Pack> list = packRepository.findAll();
        List<Pack> getList = new ArrayList<>();
        for(int i=0;i<list.size();i++){
            if(list.get(i).getState().equals("已取件")){
                getList.add(list.get(i));
            }
        }
        return getList;
    }

    public Pack setFetchTime(String name,String  fetchTime){
        Pack pack = null;
        List<Pack> list = packRepository.findAll();
        for(int i=0;i<list.size();i++){
            if(list.get(i).getName().equals(name)){
                pack = list.get(i);
                pack.setRegisterTime(fetchTime);
               return packRepository.save(pack);
            }
        }
        return null;
    }

    public List<Pack> getHavingRegister(String register) {
        List<Pack> list= packRepository.findAll();
        List<Pack> getList = new ArrayList<>();
        for(int i=0;i<list.size();i++){
            if(!list.get(i).getRegisterTime().equals("")){
                getList.add(list.get(i));
            }
        }
        return getList;
    }

    public List<Pack> getHavingNotRegister(String register) {
        List<Pack> list= packRepository.findAll();
        List<Pack> getList = new ArrayList<>();
        for(int i=0;i<list.size();i++){
            if(list.get(i).getRegisterTime().equals("")){
                getList.add(list.get(i));
            }
        }
        return getList;
    }
}
