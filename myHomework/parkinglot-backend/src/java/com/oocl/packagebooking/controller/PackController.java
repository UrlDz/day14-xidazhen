package java.com.oocl.packagebooking.controller;

import com.oocl.packagebooking.entity.Pack;
import com.oocl.packagebooking.service.PackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/pack")
public class PackController {
    @Autowired
    PackService packService;

    @PostMapping
    public Pack addPackage(@RequestBody Pack pack) {
        //System.out.println(pack.getRegisterTime());
        return packService.addPackage(pack);
    }

    @GetMapping
    public List<Pack> getAllPackage() {
        return packService.getAllPackage();
    }

    @PutMapping
    public Pack modifyPackage(@RequestBody long id) {
        System.out.println(id);
        return packService.modifyPackage(id);

    }
    @GetMapping("/11/{register}")
    public List<Pack> getHavingRegister(@PathVariable String register){
        return packService.getHavingRegister(register);
    }

    @GetMapping("/{fecth}")
    public List<Pack> getHavingPack(@PathVariable String fecth) {

        return packService.getHavingPack();
    }

    @GetMapping("/22/{register}")
    public List<Pack> getHavingNotRegister(@PathVariable String register){
        return packService.getHavingNotRegister(register);
    }

}
