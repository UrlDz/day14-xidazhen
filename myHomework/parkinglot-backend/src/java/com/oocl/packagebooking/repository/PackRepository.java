package java.com.oocl.packagebooking.repository;

import com.oocl.packagebooking.entity.Pack;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PackRepository extends JpaRepository<Pack,Long> {
}
